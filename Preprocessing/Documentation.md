# Preprocessing for Machine Learning

## Introduction

Data preprocessing comes after we've explored and cleaned our dataset.
**Example:** Transforming categorical features into numerical features (dummy variables).

### Why preprocess?
1. Transform dataset so it's suitable for modeling 
2. Improve model performance
3. Generate more reliable results

### Why split data?
1. Reduces overfitting
2. Evaluate performance on a holdout set 

#### Stratified sampling
Is a good technique for sampling more accurately when you have imbalanced classes. Is a way of sampling that takes into account the distribuition of classes in the dataset


## Standardization

Is a preprocessing method used to tranform continuous data to make it look normally distributed.                                    
Is applied to continuous numerical data.

### Log Normalization 
1. Useful for features with high variance
2. Applies logarith tranformation
3. Natural log using the constant e (=2.718)
4. $e^{3.4} = 30$
5. Captures relative changes, themagnitude of change, and keeps everuthing positive

```python
import numpy as np
df['colname'] = np.log(df['colname'])
```

### Feature scaling 
1. Features on different scales
2. Model with linear characteristics
3. Center features around 0 and transform to variance of 1

```python
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
df_scaled = pd.DataFrame(
    scaler.fit_transform(df),
    columns=df.columns)
```

## Feature Engineering
Is the creation of new features based on existing featearus, and it adds information the datasets that can improve prediction or cluestering tasks.
1. Improve performance
2. Insight into relationships between features
3. Need to understand the data first!
4. Highly dataset-dependent

Because models in scikit-learn require numerical input, if the dataset contains categorical variables, we'll have to encode them.

*Example of encode with Lambda functions with Pandas:*
```python
users["sub_enc"] = users["subscribed"].apply(lambda val: 1if val == "y"else0)
```

*Example with Scikit-Learn:*
```python
from sklearn.preprocessing import LabelEncoder

le = LabelEncoder()
users["sub_enc_le"] = le.fit_transform(users["subscribed"])
```

*Example of OneHot Encoding:*
```python
print(pd.get_dummies(users["fav_color"]))
```

### Engineering numerical features
 
#### Dates
```python
purchases["date_converted"] = pd.to_datetime(purchases["date"])
purchases['month'] = purchases["date_converted"].dt.month
print(purchases)
```

### Engineering text features
1. **Regular expressions:** Code ti identify patterns.
2. **TF/IDF:** Vectorizes words based upon importance.

## Feature Selection
- Selecting features to be used for modeling
- Doesn't create new features
- Imporve model's performance
- Reducing noise 
- Features are strongly statiscally correlated
- Reduce overall variance 

One of the easiest ways to determine if a feature is unnecessaty is to check if ti is redundant in some way.

- Remove noisy features
- Remove correlated features
- Remove duplicated features


## UFOs and preprocessing
