airflow connections add 'forex_api' \
    --conn-type "HTTP", \
    --conn-host "https://gist.github.com/" 


airflow connections add 'forex_path' \
    --conn-type "File (path)", \
    --conn-extra {'"path"':'"/opt/airflow/dags/files"'}


airflow connections add 'hive_conn' \
    --conn-type "Hive Server 2 Thrift" \
    --conn-host "hive-server" \
    --conn-login "hive" \
    --conn-password "hive" \
    --conn-port "10000"


airflow connections add 'spark_conn' \
    --conn-type "Spark", \
    --conn-host "spark://spark-master" \
    --conn-port "7077"


airflow connections add 'slack_conn' \
    --conn-type "HTTP", \
    --conn-password "https://hooks.slack.com/services/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" \
