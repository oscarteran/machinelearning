# Machine Learning
**Author:** Oscar Hernández Terán


This repository contains material developed in conjunction with the Machine Learning Scientist with Python course of the DataCamp platform.


Contact information:
- email: oscarhdzteran@gmail.com
- Social Networks:
    - [LinkedIn](https://www.linkedin.com/in/oscar-hern%C3%A1ndez-ter%C3%A1n-32b7aa208/)